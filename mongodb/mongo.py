'''
Create a fully working address book storing the data into a Mongo Database.
Each entry in the address book needs to be represented by a Document in the Mongo DB.
An address book entry needs to contain the following:
    o   First Name
    o   Last Name
    o   Email address (not mandatory, should not be stored in the document if omitted)
    o   Home and Work phone numbers (Either of them are not mandatory fields)
'''
import pymongo
from pymongo import MongoClient
from bson.json_util import dumps
from pprint import pprint
import json


class mongodbInterface(object):

    def __init__(self, document_name, collection_name, db_name, db_ip='localhost', db_port=27017):
        print(F'Connecting to database "{db_name}"...  ', end=' ')
        self.client = MongoClient(db_ip, db_port)
        self.mydatabase = self.client[db_name]
        self.mycolllection = getattr(self.mydatabase, collection_name)
        self.mydocument = getattr(self.mycolllection, document_name)
        print('Success')

    def print_row(self, r):
        '''
        pretty print each row with a blank line between each line
        '''
        print('------- Database Dump --------\n')    
        for i in r:
    #         d = dumps(i)
            pprint(i)
            print()
        print('------------------------------')
        
    def insert_new_entry(self, dict_input):
        '''
        This function takes a dictionary as input and stores it into the DB
        '''
        self.mydocument.insert_one(dict_input)
    
    def update_entry(self, firstname, lastname, dict_input):
        '''
        This function takes the first name and last name of the document to modify and a dictionary of the values to update
        Note : For the sake of this exercice, we don't allow multiple people with identical names
        '''
        dict_find = {'firstname':firstname,
                     'lastname':lastname}
        self.mydocument.update_one(dict_find, {'$set':dict_input})
    
    def display_entries(self, firstname='', lastname=''):
        '''
        This function displays all matching records for the first name and last name provided.
        Example: When searching for 'Jim', it should return entries such as: 
            1.  Jimmy
            2.  Jim
            3.  Jimbo
            4.  ...
        If both firstname and lastname are None (or not given), then the function should display all entries in alphabetical order (based on lastname)
        '''
        if (firstname == None or firstname == '') and (lastname == None or lastname == ''):
            result = self.mydocument.find({}).sort('lastname', pymongo.ASCENDING)
        else:
            dict_find = {'firstname':{'$regex':firstname},
                         'lastname':{'$regex':lastname}}
            result = self.mydocument.find(dict_find)
        self.print_row(result)
        
    def delete_entries(self, firstname=None, lastname=None):
        '''
        This function should delete all documents matching the firstname and lastname (Same as above, if multiple matches, delete multiple entries)
        '''
        if (firstname == None or firstname == '') and (lastname == None or lastname == ''):
            makesure = input('You are going to delete everythng in the document, continue? [y/n]')
            if makesure == 'y':
                result = self.mydocument.delete_many({})
            else:
                return
        else:
            dict_find = {'firstname':firstname,
                         'lastname':lastname}
            result = self.mydocument.delete_many(dict_find)
    #     return result.deleted_count

    
def test_code():
    '''
    this is the testing area
    '''
    x = mongodbInterface('addressBook', 'corporateClients', 'incubator')
    x.delete_entries()
    new_entry = {'firstname':'Nikos',
                 'lastname':'Papados',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
    
    x.insert_new_entry(new_entry)
    
#     print(delete_entries('Nikos', 'Papados'))
    upd_entry = {'firstname':'Saxlamaras',
                 'lastname':'Papados',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
    x.update_entry('Nikos', 'Papados', upd_entry)
    new_entry = {'firstname':'Usksdidfgv',
                 'lastname':'Ihysper',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
     
    x.insert_new_entry(new_entry)
    new_entry = {'firstname':'Asgrdsfg',
                 'lastname':'Lksdfglk',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
     
    x.insert_new_entry(new_entry)
    new_entry = {'firstname':'Iksow',
                 'lastname':'Pmdiw'}
     
    x.insert_new_entry(new_entry)
    new_entry = {'firstname':'Olsjgui',
                 'lastname':'JKbnsdi',
                 'email':'nikpapd@asdf.com'}
     
    x.insert_new_entry(new_entry)
    new_entry = {'firstname':'Unmsdikhnf',
                 'lastname':'Omnshnkfn',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
     
    x.insert_new_entry(new_entry)
    new_entry = {'firstname':'Ynsdkljiw',
                 'lastname':'Umsdoefj',
                 'email':'nikpapd@asdf.com',
                 'homeNumber':'12341234',
                 'workNumber':'45674567'}
     
    x.insert_new_entry(new_entry)
     
    x.display_entries(firstname='', lastname='')


test_code()
