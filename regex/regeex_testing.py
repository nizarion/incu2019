import re
from ip_validator import is_valid_ip, get_ip_class
from find_crc_types import get_error_counters
from remove_dup_words import remove_duplicates
from vlan_db import get_vlan_db

# clear_text = remove_duplicates(' this is a very nice nice text')
# 
# print (clear_text)

valid12 = is_valid_ip('165.138.7.160')
 
print(valid12)

'''
^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9]))$
'''
