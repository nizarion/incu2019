
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################
import re


def is_valid_ip(ip):
	# write here 
	valid_flag = False
	pattern = re.compile(r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$") 
	result = pattern.match(ip)
	if result is not None:
		valid_flag = True
	return valid_flag


def get_ip_class(ip):
	# write here
	if not is_valid_ip(ip):
		return 'That is not a valid IP'
	
	# Class A	1.0.0.1 to 126.255.255.254	Supports 16 million hosts on each of 127 networks.
	classAreg = re.compile(r"") 
	# Class B	128.1.0.1 to 191.255.255.254	Supports 65,000 hosts on each of 16,000 networks.
	classBreg = re.compile(r"") 
	# Class C	192.0.1.1 to 223.255.254.254	Supports 254 hosts on each of 2 million networks.
	# Does not match for 223.122.255.255
	classCreg = re.compile(r"^(?:(?:19[2-9]|2[0-1][0-9]|22[0-3])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-4]|2[0-4][0-9]|[01]?[0-9]?[1-9])\.)(?:(?:25[0-4]|2[0-4][0-9]|[01]?[0-9]?[1-9]))$")  
	# Class D	224.0.0.0 to 239.255.255.255	Reserved for multicast groups.
	classDreg = re.compile(r"^(?:(?:22[4-9]|23[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9]))$") 
	# Class E	240.0.0.0 to 254.255.255.254	Reserved for future use, or research and development purposes.
	classEreg = re.compile(r"^(?:(?:25[0-4]|2[4][0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9]?[0-9])\.)(?:(?:25[0-4]|2[0-4][0-9]|[01]?[0-9]?[0-9]))$") 
	
	return_text = 'class d'
	return return_text

