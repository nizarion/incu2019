
###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################
import re


def remove_duplicates(text):
# 	# write here
# 	pattern = re.compile(r'(\w+)\1', r'\1') 
# 	string = text
# 	text_with_no_dup = pattern.match(string)
# 	
# 	print (text_with_no_dup.group())  # will print out 'r'
	
	text_with_no_dup = re.sub(r'\b(\w+)( \1\b)+', r'\1', text)
	
	return text_with_no_dup

