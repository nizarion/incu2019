'''
Created on 17 Dec 2018
The scope of the homework is to write a python library which will allow Network 
Administrators to accelerate the automation in a Cisco Nexus 9000 based network, 
while leveraging NX-API CLI and/or NX-API REST in the Open NX-OS.

* Python 3.6+
* list all requirements needed for your library
* docstring/documentation present
* the name of the library will be `nxostoolkit`
* using your library, users will have access to a class named `nexus`
* the class will have 3 methods

REQUIREMENTS
    modules:
        requests
        json
    information
@author: Nizar
'''

import requests
import json


class Nexus(object):
    '''The class has 2 attributes:
        * version: str representing the version running on the switch
        * platform: str representing the model of the switch. This detail can be
                    found in the _**show version**_ output (chassis_id).
    '''

    def __init__(self, ip_new="10.48.74.237"):
        self.lab_nexus = {"ip": "10.48.74.237",
                     "port": "80",
                     "user": "admin",
                     "pass": "cisco!123"}
        self.uri = 'http://{}:{}/ins'.format(self.lab_nexus['ip'], self.lab_nexus['port'])
        self.my_headers = {'content-type': 'application/json-rpc'}
        self.version = ''
        self.hostname = ''
        self.platform = ''
        self.lab_nexus['ip'] = ip_new
        # Interactive prompt
        # newip = input('What is the ip of the server? (press enter for default)')
        # if newip != '':
        #    self.lab_nexus['ip'] = newip
            
    def authenticate(self, in_user, in_password):
        ''' * details: used to auth to a switch
            * input: user: str, 
                     pass: str
            * ouptut: None
        '''
        self.lab_nexus["user"] = in_user
        self.lab_nexus["pass"] = in_password
        self.payload = [{"jsonrpc": "2.0",
                       "method": "cli",
                       "params": {"cmd": "show version",
                       "version": 1},
                       "id": 1}
         ]
        
        self.response = requests.post(self.lab_nexus['ip'], data=json.dumps(self.payload), headers=self.my_headers, auth=(self.lab_nexus["user"], self.lab_nexus["pass"])).json()
        
        self.payload = [{"jsonrpc": "2.0",
                       "method": "cli",
                       "params": {"cmd": "show version",
                       "version": 1},
                       "id": 1}
         ]
        # Process the response
        self.version = self.response['result']['body']['kickstart_ver_str']
        self.platform = self.response['result']['body']['chassis_id']
        self.hostname = self.response['result']['body']['host_name']
        
        # print "ip : {0} is a \"{1}\" with hostname: {2} running software version : {3}".format(self.lab_nexus['ip'] , self.platform, self.hostname, self.version)
        
    def get_interface_status(self, if_name):
        ''' * details: will return the status of specified interface
            * input: if_name: str, representing interface names (Note: interface
                    name can be "Eth1/1" or "Ethernet1/1" or "Ethernet 1/1")
            * output: string representing the state: "up", "down", "unknown" 
        '''
        self.payload = [{"jsonrpc": "2.0",
          "method": "cli",
          "params": {"cmd": "show interface " + if_name,  # Ethernet1/1",
                     "version": 1},
          "id": 1}
         ]
        
        self.response = requests.post(self.lab_nexus['ip'], data=json.dumps(self.payload), headers=self.my_headers, auth=(self.lab_nexus["user"], self.lab_nexus["pass"])).json()
        
        # Return interface status
        if self.response["state"] == 'up':
            return 'up'
        elif self.response["state"] == 'down':
            return 'down'
        elif self.response["state"] == 'unknown':
            return 'unknown'
        else:
            return 'Invalid interface name'

    def configure_interface_desc(self, if_name, desc_string):
        ''' * details: method will configure the description of the interface
            * input: if_name: str, 
                     description: str; Note: interface name can be
                      "Eth1/1" or "Ethernet1/1" or "Ethernet 1/1"
            * output: None
        '''
        self.payload = [{"jsonrpc": "2.0",
                          "method": "cli",
                          "params": {"cmd": "configure terminal ; interface " + if_name + " ; description " + desc_string, #'This is a physical interface'",  # Ethernet1/1",
                                     "version": 1},
                          "id": 1}
         ]
        
        self.response = requests.post(self.lab_nexus['ip'], data=json.dumps(self.payload), headers=self.my_headers, auth=(self.lab_nexus["user"], self.lab_nexus["pass"])).json()
        
