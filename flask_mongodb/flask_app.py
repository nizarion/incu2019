'''
The goal of this exercise is to create a fully working address book. This needs to be done using Flask, REST API calls and JSON encoding. 

The program needs to have the basic functionalities: 

1)    Each entry in the address book must contain the following attributes: 
a.    Userid
b.    Name
c.    Phone number
d.    Email
'''
from flask import Flask, render_template, request, Markup, make_response, jsonify
import mongo
from bson.json_util import dumps

app = Flask(__name__)

database = {'usrID':['Name', 'Phone Number', 'Email']}

x = mongo.mongodbInterface('addressBook', 'flask_app_v1', 'incubator')


@app.route('/list', methods=['GET'])
def list():
    '''
    2)    List all elements <GET call, output in JSON>
    '''
    a = dumps(x.display_entries())
    # print(a)
    return jsonify(a)


@app.route('/list/<id>', methods=['GET'])
def list_byID(id):
    '''
    3)    List a single element identified by id <GET call, output in JSON>
    '''
    a = dumps(x.id_str_to_obj(id))
    # print(a)
    return jsonify(a)


@app.route('/edit/<id>', methods=['PUT'])
def edit_byID(id):
    '''
    4)    Edit a single element identified by id <PUT call, input in JSON, output in JSON>. 
            Must be able to edit a single attribute or multiple attributes at once.

    '''
    payload = request.get_json()
    x.update_entry_byID(id, payload)
    return (list_byID(id))

# @app.route("/add_entry/<firstname>/<homeNumber>/<email>", methods=["POST"])
# def add_entry(firstname, homeNumber, email):


@app.route("/add_entry", methods=["POST"])
def add_entry():
    '''
    5)    Submit a new element <POST call, body in JSON>
    '''
#     If data is sent as JSON
    if request.get_json() is not None:
        new_entry = {'firstname':request.get_json()['firstname'],
                 'email':request.get_json()['email'],
                 'homeNumber':request.get_json()['homeNumber']}
        x.insert_new_entry(new_entry)
        return "JSON encoded - Title: %s Content : %s" % (request.get_json()['firstname'], request.get_json()['email'])
    # If data is sent as FORM encoded
    else:
        return "FORM encoded - Title: %s Content : %s" % (request.form['firstname'], request.form['email'])


@app.route('/delete/<id>', methods=['DELETE'])
def delete(id):
    '''
    6)    Delete a single element identified by id <DELETE call>
    '''
    deleted_user = x.delete_entry_byID(id)
    return "Deleted: {} \n".format(deleted_user)


def test_code1():
    new_entry = {'firstname':'Nikos',
             'lastname':'Papados',
             'email':'nikpapd@asdf.com',
             'homeNumber':'12341234'}
    
    print(x.insert_new_entry(new_entry))
    print(x.id_str_to_obj('5c48fd0799e7e606004d8aab'))
    x.update_entry_byID('5c48fd0799e7e606004d8aab', firstname='anpter', email='asdf@newmail.com')
    print(x.id_str_to_obj('5c48fd0799e7e606004d8aab'))

# test_code1()
'''
7)    Flask app listening on port 7676
'''
app.run(port=7676)
